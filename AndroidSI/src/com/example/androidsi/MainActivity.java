package com.example.androidsi;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {

	EditText etNum1;
	EditText etNum2;
	EditText etNum3;

	Button Calculate;
	

	TextView Result1;
	TextView Result2;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// find the elements
		etNum1 = (EditText) findViewById(R.id.value_p);
		etNum2 = (EditText) findViewById(R.id.value_n);
		etNum3 = (EditText) findViewById(R.id.value_r);

		Calculate = (Button) findViewById(R.id.Calculate);
		
		Result1 = (TextView) findViewById(R.id.Result1);
		Result2 = (TextView) findViewById(R.id.Result2);
		// set a listener
		Calculate.setOnClickListener(this);
		

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		float num1 = 0;
		float num2 = 0;
		float num3 = 0;
		float result1 = 0;
		float result2 = 0;
		// check if the fields are empty
		if (TextUtils.isEmpty(etNum1.getText().toString())
				|| TextUtils.isEmpty(etNum2.getText().toString())
				|| TextUtils.isEmpty(etNum3.getText().toString())) {
			return;
		}

		// read EditText and fill variables with numbers
		num1 = Float.parseFloat(etNum1.getText().toString());
		num2 = Float.parseFloat(etNum2.getText().toString());
		num3 = Float.parseFloat(etNum3.getText().toString());

		// defines the button that has been clicked and performs the
		// corresponding operation
		// write operation into oper, we will use it later for output
		//switch (v.getId()) {
		//case R.id.Calculate:

			result1 = (num1 * num2 * num3) / 100;
			result2 = result1 + num1;
			//break;
		
		//

		// form the output line
		Result1.setText("SI:"+result1 +"");
		Result2.setText("Total:"+result2+"");

	}
}