
import java.util.Arrays;
import java.util.HashSet;


public class MergeArray {

	public static int[] mergeArray(int[] array1,int[] array2)
	{
		
		 int[] array3=new int[array1.length+array2.length];
		 System.arraycopy(array1, 0, array3, 0, array1.length);
		 System.arraycopy(array2, 0, array3, array1.length,array2.length);
		 
		  return array3;
				
	}
	
	
	public static void main(String[] args)
	{
		int[] array1={5,5,6,6,6,1,1,1,1};
		int[] array2={4,3,7};
		int[] out=mergeArray(array1,array2);
		System.out.println("Merged Array:" + Arrays.toString(out));
		HashSet hs=new HashSet();
		 hs.add(out);
		 System.out.println("values in hash set:"+ (out));
		
		Arrays.sort(out);
		System.out.println("Sorted Array:" + Arrays.toString(out));
		
	}
}
