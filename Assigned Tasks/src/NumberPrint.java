public class NumberPrint {

	public static void main(String args[])

	{
		int limit = 50;

		System.out.println("Printing Numbers :" + limit);

		for (int i = 1; i <= limit; i++) {
			if ((i % 3 == 0) && (i % 5 == 0)) {
				System.out.println("Three-Five");
			} else if ((i % 3 == 0)) {
				System.out.println("Three");
			} else if ((i % 5 == 0)) {
				System.out.println("Five");
			} else {

				System.out.println(+i);
			}

		}

	}

}
