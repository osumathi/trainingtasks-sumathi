import java.util.Scanner;

public class RemoveDuplicates {
	static int[] removeDuplicates(int arr[]) {
		int[] tempa = new int[arr.length];
		int uniqueCount = 0;
		for (int i = 0; i < arr.length; i++) {
			boolean unique = true;
			for (int j = 0; j < uniqueCount && unique; j++) {
				if (arr[i] == tempa[j]) {
					unique = false;
				}
			}
			if (unique) {
				tempa[uniqueCount++] = arr[i];

			}
		}
		int[] array2=new int[uniqueCount];
		
		for(int i=0;i<array2.length;i++)
		{
			array2[i]=tempa[i];
		}
		
		return array2;

	}

	public static void main(String[] args) {
		int[] array = new int[5];
		System.out.println("Values:");
		Scanner in = new Scanner(System.in);
		for (int j = 0; j < array.length; j++)

		{
			array[j] = in.nextInt();

		}

		System.out.println("Duplicate Array:");
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}

		System.out.println("Without Duplicate Array:");

		int[] array1 = removeDuplicates(array);

		for (int i = 0; i < array1.length; i++) {
			
			System.out.println(array1[i]);
			}
			
			
		}

		
	}

